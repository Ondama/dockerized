# Kubernetes tooling

This is a setup to be used with EKS projects.

Includes:
---
- `aws` - AWS CLI
- `kubectl` - Container management CLI
- `kubestash` - Secret management CLI powered by `credstash`
- `aws-iam-authenticator` - AWS IAM Authenticator for Kubernetes

## Available commands
1. `make` or `make image` - Create the docker image with the
appropriate tooling
2. `make <cluster name>.yml` - Create an updated kubeconfig file for a
cluster
3. `make run CLUSTER=<cluster name>` - Open a shell on the container
with the kubeconfig of the specified cluster as the default
4. `make clean` - Delete all the config files and docker images

These commands can also receive variable overrides, useful to use
different AWS profiles or regions.