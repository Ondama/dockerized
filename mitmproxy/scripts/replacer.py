import os
import re
from base64 import b64encode
from typing import Callable, Optional, Set, Tuple

from mitmproxy import ctx, http
from mitmproxy.addonmanager import Loader
from mitmproxy.proxy.protocol.http import HTTPMode
from mitmproxy.script import concurrent


def load_lines(file_path: str, transform: Callable = None) -> set:
    data = set()
    with open(file_path, "r") as file_data:
        for line in file_data:
            line = line.rstrip("\n")
            data.add(transform(line) if transform else line)
    return data


class JSReplacer:

    MIME_TYPES = {"js": "application/javascript", "map": "application/json"}
    REQUEST_PATH_EXP = re.compile(r"^/([^/]+)\.(js|map)$")
    FILE_PATH_TMPL = "%s/{}.{}" % os.getenv("DIST_FILES_PATH")

    UPSTREAM_HOST = None
    UPSTREAM_AUTH = None
    UPSTREAM_RULE_EXP = load_lines("%s/rules" % os.getenv("UPSTREAM_CONFIG_PATH"), re.compile)
    UPSTREAM_BYPASS_LST = load_lines("%s/whitelist" % os.getenv("UPSTREAM_CONFIG_PATH"))

    FILE_DATA = {}  # File cache
    REPLACEMENT_TARGET = None  # Domain in which to replace files

    @classmethod
    def read_data(cls, path: str) -> str:
        if not cls.FILE_DATA.get(path, None):  # TODO: Add option to ignore cache
            if not os.path.isfile(path):
                return None  # fails silently
            with open(path, "r") as file_data:
                cls.FILE_DATA[path] = file_data.read()
        return cls.FILE_DATA[path]

    @classmethod
    def replace_js(cls, host: str, path: str) -> Tuple[str, str]:
        if host == cls.REPLACEMENT_TARGET:
            match = cls.REQUEST_PATH_EXP.match(path)
            if match:
                path = match.group(1)
                extension = match.group(2)
                mime_type = cls.MIME_TYPES[extension]
                local_path = cls.FILE_PATH_TMPL.format(path, extension)
                return cls.read_data(local_path), mime_type
        return (None, None)

    @classmethod
    def should_use_upstream(cls, host: str) -> bool:
        if not cls.UPSTREAM_HOST:
            return False
        for rule in cls.UPSTREAM_RULE_EXP:
            if rule.match(host) and host not in cls.UPSTREAM_BYPASS_LST:
                return True
        return False

    @staticmethod
    def load(loader: Loader) -> None:
        loader.add_option("target_domain", Optional[str], None, "Target for js replacements")
        loader.add_option("upstream_host", Optional[str], None, "Upstream proxy domain")
        loader.add_option("upstream_port", Optional[int], 80, "Upstream proxy port")

    @classmethod
    def configure(cls, updated: Set[str]) -> None:
        if "target_domain" in updated:
            cls.REPLACEMENT_TARGET = ctx.options.target_domain
            ctx.log.info("Replacing js on '%s'" % cls.REPLACEMENT_TARGET)
        if "upstream_host" in updated or "upstream_port" in updated:
            cls.UPSTREAM_HOST = (ctx.options.upstream_host, ctx.options.upstream_port)
            ctx.log.info("Forward to '%s:%i'" % cls.UPSTREAM_HOST)
        if "upstream_auth" in updated:
            cls.UPSTREAM_AUTH = b"Basic " + b64encode(ctx.options.upstream_auth.encode())
            ctx.log.info("Using credentials '%s'" % ctx.options.upstream_auth)

    @classmethod
    @concurrent
    def http_connect(cls, flow: http.HTTPFlow) -> None:
        if flow.live and cls.should_use_upstream(flow.request.host):
            flow.request.headers["Proxy-Authorization"] = cls.UPSTREAM_AUTH
            flow.live.mode = HTTPMode.upstream
            flow.live.change_upstream_proxy_server(cls.UPSTREAM_HOST)

    @classmethod
    @concurrent
    def request(cls, flow: http.HTTPFlow) -> None:
        (alt_content, mime_type) = cls.replace_js(flow.request.host, flow.request.path)
        if alt_content:
            flow.response = http.HTTPResponse.make(
                200,  # http status code
                alt_content,  # body
                {"Content-Type": "%s" % mime_type, "X-Powered-By": "mitmproxy"},  # headers
            )


addons = [JSReplacer()]
