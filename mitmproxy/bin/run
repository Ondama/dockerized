#!/usr/bin/env bash

DOCKER_REGISTRY=registry.gitlab.com
IMAGE_NAME=ondama/dockerized/proxy-tools
LATEST_IMAGE=$DOCKER_REGISTRY/$IMAGE_NAME

ROOT_DIR=$(cd $(dirname $(dirname $0)); pwd)

DOCKER_VOLUMES=(
  "-v $ROOT_DIR/certs:/home/mitmproxy/.mitmproxy"
  "-v $ROOT_DIR/data:/home/mitmproxy/data"
  "-v $ROOT_DIR/upstream:/home/mitmproxy/upstream"
)

bold=$(tput bold)
normal=$(tput sgr0)

PORT=8080
PROXY_ARGS=""
HOST_PORT=""

function usage () {
  local name=$(basename $0)
  echo "${bold}NAME${normal}"
  echo -e "\t${name} - start proxy tools"
  echo -e "\n${bold}SYNOPSIS${normal}"
  echo -e "\t${name} [-p|--port <number>] [-u|--upstream] [-h|--help]"
  echo -e "\n${bold}DESCRIPTION${normal}"
  echo -e "\tStart a proxy for debugging purposes"
  echo -e "\tUse the ${bold}upstream${normal} option when the target is behind a proxy"
  echo -e "\tDefault port is: ${bold}${PORT}${normal}"
  exit 0
}

function start_proxy () {
  echo -e "\nStarting..."
  local DOCKER_ARGS="-p $HOST_PORT:8080 --rm -it ${DOCKER_VOLUMES[@]}"
  exec docker run $DOCKER_ARGS $LATEST_IMAGE start_proxy $PROXY_ARGS
}

function leave () {
    echo -e "\nBye..."
    exit 0
}

SHORT_OPTS="p:uh"
LONG_OPTS="port:,upstream,help"

PARSED_ARGS=$(getopt --options=$SHORT_OPTS --longoptions=$LONG_OPTS --name "$0" -- "$@" 2>/dev/null)
[ $? -ne 0 ] && usage

eval set -- "$PARSED_ARGS"
while true; do
  case "$1" in
    -u|--upstream) PROXY_ARGS+=" upstream"; shift;;
    -p|--port) HOST_PORT="$2"; shift 2;;
    -h|--help) usage; shift;;
    --) shift; break;;
    *) shift;;
  esac
done

HOST_PORT=${HOST_PORT:-$PORT}

read -n1 -rsp "Start${bold}${PROXY_ARGS}${normal} proxy on ${bold}${HOST_PORT}${normal}? [y/N] " opt
[[ $opt =~ (Y|y) ]] && start_proxy || leave
