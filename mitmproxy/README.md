# Proxy tooling

This is setup can be used to debug web applications and inspect network
traffic.

Scripts included:
---
- `replacer.py` - Tool to replace JS scripts and source maps on live
sites with local versions

## Available commands
1. `bin/build` - Create the docker image with the appropriate tooling
2. `bin/run` - Start the tool
    - `bin/run -h` - See the help menu

**NOTE:**
To forward requests to another proxy, two files need to be created on
the _upstream_ folder:
- _rules_ - Each line should be a regular expression. Requests made to
a domain matching any of the expressions will be forwarded unless they
are whitelisted.
- _whitelist_ - Each line should have a hostname (no wildcards).